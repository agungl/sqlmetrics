# Example - Oracle Instance Monitor

This example shows how you can monitor an Oracle database instance using
the SQL-based Metrics Provider, Prometheus and Grafana.

Core components are the definition of the database metrics ([`config.yml`](config.yml))
and the related dashboard for the visualization ([`grafana/dashboards/oracle_instance_overview.json`](grafana/dashboards/oracle_instance_overview.json)).
Anything else is normal configuration stuff to run the database and the other needed tools
for the monitoring as Docker containers.

Here is a screenshot of the dashboard:

![Screenshot](Screenshot.png)

## Running the Example

Open a console and go to the `example` directory.
Then start the containers using `docker-compose` like this:

    docker-compose up [-d]

If you omit the `-d` parameter, you can follow the log output of all containers in the console.

You can reconfigure the port mapping for the containers in the `docker-compose.yml` file individually.
By default, the web UI of Grafana is available via [http://localhost:13000](http://localhost:13000).
The credential for Grafana are _admin / secret_.
Navigate to the dashboards and choose the preinstalled **Oracle Instance Overview** dashboard.

If you want to have insights into the Prometheus service, you can use [http://localhost:19090](http://localhost:19090).
The collected metrics of the SQL-based Metrics Provider can be retrieved from [http://localhost:18080/metrics](http://localhost:18080/metrics).

Please not, the Oracle database instance takes between 30 sec and 1 min to be up and running.
During that time, the monitor container will stop with a connection error.
It restarts automatically, though.

To stop the running containers, open a console and go to the `example` directory.
Here you use:

    docker-compose stop
    docker-compose down

## Modifications

### Change or Extend Metrics

The Metrics are defined in the file `config.yml`.
If you modify or add metrics, you should restart the container instance for the SQL-based Metrics Provider:

    docker-compose stop monitor
    docker-compose start monitor

### SQL-based Metrics Provider Instances

The example contains one Oracle database instance and one Metrics Provider instance for that database.
If you like, you can add more Metrics Provider services to the `docker-compose.yml` file.
You could even add other datbase servers like used in the local development environment.
If you make any changes to the file, don't forget to restart the setup.

And please keep in mind to adopt the scrape configuration of Prometheus.
You should add entries for newly added Metrics Provider services to the 
[`prometheus/etc/prometheus.yml`](prometheus/etc/prometheus.yml) file.
To make Prometheus aware of a modification, restart the container like this:

    docker-compose stop prometheus
    docker-compose start prometheus

### Change the Dashboard

The dashboard is defined in a JSON file which is initally loaded by Grafana during startup.
You can modify the dashboard in the Grafana web UI.
If you want to persist the changes, please expoert the dashboard as JSON file and replace the example file
[`grafana/dashboards/oracle_instance_overview.json`](grafana/dashboards/oracle_instance_overview.json).