package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jackc/pgx/v5/stdlib"
	_ "github.com/microsoft/go-mssqldb"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	_ "github.com/sijms/go-ora/v2"
	"gitlab.com/agungl/sqlmetrics/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// version of the application
var (
	version    = "0.0.0"
	buildstamp = "unknown"
	githash    = "unknown"
)

// pointer to the SQL instance used to access the database
var dbInstance *sql.DB

// parsed configuration of the custom metrics to get read from the DB
var configEntries *utils.ConfigEntries

// address of the HTTP endpoint
var addr = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")

// name of the configuration file for the metrics
var metricConfig = flag.String("metric-config", "config.yml", "The configuration file containing the metrics definitions. (Overwritten by $CONFIG)")

// name of the configuration file for the DB access
var dbConfig = flag.String("db-config", "", "The configuration file containing the DB credentials. (Overwritten by $DB_*)")

// switch for the activation of showing all collected metric values in the log
var verbose = flag.Bool("verbose", false, "Show the metrics values when collected. (Overwritten by any $VERBOSE value)")

// name constant for logging:
const fldQuery = "query"

func configureFromEnv() {
	var logger *zap.Logger
	envVerbose := os.Getenv("VERBOSE")
	if (len(envVerbose) > 0) || *verbose {
		config := zap.NewDevelopmentConfig()
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
		logger, _ = config.Build()
	} else {
		logger, _ = zap.NewProduction()
	}
	zap.ReplaceGlobals(logger)
	logger.Info(fmt.Sprintf("SQL-based Metrics Provider v%s", version))
	logger.Debug(fmt.Sprintf("UTC Build Time: %s", buildstamp))
	logger.Debug(fmt.Sprintf("Git Commit Hash: %s", githash))
}

// Initialization for Prometheus metrics
//
// Here all metrics need to get registered.
// At first, we read the configuration from a file.
// Then we iterate over the configuration entries.
// Metrics of the types Counter and Gauge get initialized and registered.
// The instances are kept for later access in global maps.
//
// If a metric has labels, it is stored in vectors.
// Depending on the configuration it is necessary to initialize
// either a single metric or a vector.
func setupMetrics() {
	// create the configuration and read the data
	configEntries = &utils.ConfigEntries{}
	configEntries.ReadConfig(*metricConfig)
	zap.L().Info("Reading metric definitions from the configuration finished", zap.Int("count", len(*configEntries)))
	// process configuration entries
	for i, entry := range *configEntries {
		if entry.Query == "" && entry.Name != "" {
			zap.S().Warnf(
				"Please migrate the definition of the metric for '%s' to the new format to allow the retrieval of the values!",
				entry.Name)
			// use "Name" for error counter and for logging of the initialization to avoid an empty string
			(*configEntries)[i].Query = entry.Name
		}
		utils.RegisterMetrics(&((*configEntries)[i]))
	}
}

// Start of the processing by the creation of the DB connection.
// The application terminates if no DB connection is available.
// Then the cyclic background processing for the data retrieval is initiated.
// The service for the provision of the metric data for the Prometheus
// scraper runs in the foreground. If it terminates, the whole processing ends.
func main() {
	// read start parameters
	flag.Parse()
	// take care of ENV variables which might overwrite settings
	configureFromEnv()
	defer func() {
		if err := zap.L().Sync(); err != nil {
			zap.L().Error("Error during sync of the logger", zap.Error(err))
		}
	}()

	// read metric definitions
	setupMetrics()
	// create a DB connection according to the provided configuration values
	var dbCfg utils.DbConfig
	dbCfg.InitDbConfig(*dbConfig)

	db, err := utils.Connect(dbCfg)
	if err != nil {
		// DB connection not initiated, process terminates after output of the error
		zap.L().Fatal("DB connection creation failed", zap.Error(err))
	} else {
		// DB connection initiated
		zap.L().Info("DB connection created")
		dbInstance = db
		defer func() {
			if err := db.Close(); err != nil {
				zap.L().Error("Error while closing the database", zap.Error(err))
			}
		}()

		// wait time for the cyclic data retrieval
		globalSleepTime := calcGlobalSleepTime()

		// DataFetcher instance for SQL
		dataFetcher := utils.SqlDataFetcher{DbInstance: dbInstance}

		// start the background routines for data retrieval
		for _, v := range *configEntries {
			config := v
			metricSleepTime := calcMetricSleepTime(globalSleepTime, config)
			go func() {
				for {
					utils.ProcessConfig(dataFetcher, &config)
					time.Sleep(time.Duration(metricSleepTime) * time.Second)
				}
			}()
		}
		// provide the metrics data via an HTTP endpoint, the service runs until cancellation with SIGTERM
		srv := http.Server{
			Addr:              *addr,
			ReadTimeout:       1 * time.Second,
			WriteTimeout:      3 * time.Second,
			IdleTimeout:       30 * time.Second,
			ReadHeaderTimeout: 2 * time.Second,
		}
		http.Handle("/metrics", promhttp.Handler())
		http.HandleFunc("/health", healthHandler)
		zap.L().Fatal("HTTP server is terminated", zap.Error(srv.ListenAndServe()))
	}
}

// Determination of the global sleep time and consider
// a potential value in the SLEEP environment variable
func calcGlobalSleepTime() int {
	globalSleepTime := 1
	setting := os.Getenv("SLEEP")
	sleepValue, err := strconv.Atoi(setting)
	if (err != nil) || (sleepValue < 1) {
		zap.L().Warn("Invalid or no update intervall defined", zap.String("value", setting))
	} else {
		globalSleepTime = sleepValue
	}
	zap.L().Info("Setting global update intervall", zap.Int("seconds", globalSleepTime))
	return globalSleepTime
}

// Determination of the concrete sleep time for a query
// by comparing the global and specific intervall for the query.
func calcMetricSleepTime(globalSleepTime int, config utils.ConfigEntry) int {
	metricSleepTime := config.Sleep
	if metricSleepTime < 1 {
		metricSleepTime = globalSleepTime
		zap.L().Info("Starting routine for data retrieval", zap.String(fldQuery, config.Query))
	} else {
		zap.L().Info("Starting routine for data retrieval with individual sleep time",
			zap.String(fldQuery, config.Query),
			zap.Int("sleepTime", metricSleepTime))
	}
	return metricSleepTime
}

// Report the health of the service to the outside
func healthHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "application/health+json")
	w.Header().Set("cache-control", "max-age=30")
	jsonResp, statusCode := checkHealth()
	if _, err := w.Write(jsonResp); err != nil {
		zap.L().Warn("Could not write response body", zap.Error(err))
	}
	if statusCode != http.StatusOK {
		w.WriteHeader(statusCode)
	}
}

// Prepare the health status depending on the status of the DB connection
func checkHealth() ([]byte, int) {
	statusCode := http.StatusOK
	status := make(map[string]string)
	status["status"] = "pass"
	status["version"] = version
	status["release"] = buildstamp
	status["description"] = "SQL-based Metrics Provider"

	if err := dbInstance.Ping(); err != nil {
		status["status"] = "fail"
		status["output"] = "database connection mot available"
		statusCode = http.StatusServiceUnavailable
	}

	jsonResp, err := json.Marshal(status)
	if err != nil {
		zap.L().Error("Could not marshal the health status to JSON", zap.Error(err))
		jsonResp = []byte(`{"status":"fail"}`)
	}
	return jsonResp, statusCode
}
