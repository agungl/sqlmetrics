package utils

import (
	"context"
	"os"
	"testing"
	"time"

	assertion "github.com/stretchr/testify/assert"
)

func TestReadValuesAndLabels(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestPgDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	dataFetcher := SqlDataFetcher{
		DbInstance: db,
	}
	os.Unsetenv("CONFIG")
	configEntries := &ConfigEntries{}
	configEntries.ReadConfig("test_config.yml")
	assert.Equal(2, len(*configEntries))
	entry := (*configEntries)[0]

	metrics1, labels1, err1 := dataFetcher.ReadValuesAndLabels(&entry)
	assert.Equal(1, len(metrics1))
	assert.Equal(1, len(metrics1[0]))
	assert.True(metrics1[0][0] > 0)
	assert.Equal(1, len(labels1))
	assert.Equal(0, len(labels1[0]))
	assert.Nil(err1)

	entry.Sql = "SELECT extract(epoch from current_timestamp - pg_postmaster_start_time()) AS uptime FROM DUAL"
	metrics2, labels2, err2 := dataFetcher.ReadValuesAndLabels(&entry)
	assert.Equal(0, len(metrics2))
	assert.Equal(0, len(labels2))
	assert.NotNil(err2)

	assert.Nil(db.Close())
}
