package utils

// Add a set of new labels to the collection of all recently used labels
//
func addLabelSet(collection LabelCollection, labels []string) LabelCollection {
	newCollection := append(collection, labels)
	return newCollection
}

// Check if a given set of labels is already present in the collection of all used labels
//
func containsLabelSet(collection LabelCollection, labels []string) bool {
	found := false
	for _, labelSet := range collection {
		if len(labelSet) == len(labels) {
			equal := true
			for i := 0; i < len(labelSet); i++ {
				if labelSet[i] != labels[i] {
					equal = false
				}
			}
			if equal {
				found = true
			}
		}
	}
	return found
}
