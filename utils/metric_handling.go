package utils

import (
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
)

// RegisterMetrics handles the registration of the metrics
// of a configuration entry at the Prometheus Client.
func RegisterMetrics(entry *ConfigEntry) {
	for i, metric := range entry.Metrics {
		entry.Metrics[i].Registered = true
		switch metric.Type {
		case metricTypeGauge:
			if len(entry.Labels) < 1 {
				// Gauge without labels
				zap.L().Info("Registering Gauge", zap.String(fldGauge, metric.Name),
					zap.String(fldStrategy, metric.Strategy))
				gauge := prometheus.NewGauge(
					prometheus.GaugeOpts{
						Name: metric.Name,
						Help: metric.Help,
					},
				)
				prometheus.MustRegister(gauge)
				Gauges[metric.Name] = gauge
			} else {
				// Gauge with labels
				zap.L().Info("Registering GaugeVec", zap.String(fldGauge, metric.Name),
					zap.Strings(fldLabels, entry.Labels), zap.String(fldStrategy, metric.Strategy))
				gaugeVec := prometheus.NewGaugeVec(
					prometheus.GaugeOpts{
						Name: metric.Name,
						Help: metric.Help,
					},
					entry.Labels,
				)
				prometheus.MustRegister(gaugeVec)
				GaugeVecs[metric.Name] = gaugeVec
			}
		case metricTypeCounter:
			if len(entry.Labels) < 1 {
				// Counter without labels
				zap.L().Info("Registering Counter", zap.String(fldCounter, metric.Name),
					zap.String(fldStrategy, metric.Strategy))
				counter := prometheus.NewCounter(
					prometheus.CounterOpts{
						Name: metric.Name,
						Help: metric.Help,
					},
				)
				prometheus.MustRegister(counter)
				Counters[metric.Name] = counter
			} else {
				// Counter with labels
				zap.L().Info("Registering CounterVec", zap.String(fldCounter, metric.Name),
					zap.Strings(fldLabels, entry.Labels), zap.String(fldStrategy, metric.Strategy))
				counterVec := prometheus.NewCounterVec(
					prometheus.CounterOpts{
						Name: metric.Name,
						Help: metric.Help,
					},
					entry.Labels,
				)
				prometheus.MustRegister(counterVec)
				CounterVecs[metric.Name] = counterVec
			}
		default:
			entry.Metrics[i].Registered = false
			zap.L().Error("Metric with unknown type cannot get registered!",
				zap.String(fldCounter, metric.Name), zap.String(fldType, metric.Type))
		}
	}
}

// ProcessConfig processes a metric based on their configuration
//
// Values for a metric are read from the DB and applied based on the
// configuration entry for the query.
//
// Please note that counter metrics will be incremented by read values.
// Using gauge metrics is recommended when absolute values are read!
//
// The internal monitoring counts errors during the DB access per metric.
// Additionally, the duration for a query is recorded.
// Both values can be used later like any other metric.
//
// Each metric has an associated strategy which defines what happens
// if no data is retrieved any more for a metric without labels or
// for a metric with an already known combination of labels.
//
//	drop: The metric get s removed or deregistered.
//	zero: The metric is reset to the value zero (0.0).
//	keep: This is the default behavior. The east recent value is kept.
func ProcessConfig(dataFetcher DataFetcher, entry *ConfigEntry) {
	// Start the timer for the query and create internal duration metric
	timer := prometheus.NewTimer(sqlDurations.WithLabelValues(entry.Query))
	defer timer.ObserveDuration()
	// Execute the query
	values, labels, err := dataFetcher.ReadValuesAndLabels(entry)
	if err == nil {
		entry.ReadCount += 1
		ApplyDataset(entry, values, labels)
	}
}

// ApplyDataset writes the retrieved values in the arrays according to the
// defined ConfigEntry into the related metrics.
func ApplyDataset(entry *ConfigEntry, values [][]float64, labels [][]string) {
	for i := range entry.Metrics {
		applyMetricStrategy(entry, &(entry.Metrics[i]), labels)
	}
	// iteration over the result set records
	for n := 0; n < len(values); n++ {
		// metrics per record
		for i, metric := range entry.Metrics {
			// set the value for the current metric
			switch metric.Type {
			case metricTypeGauge:
				processGaugeMetric(metric.Name, values[n][i], labels[n])
			case metricTypeCounter:
				processCounterMetric(metric.Name, values[n][i], labels[n])
			default:
				retrievalErrors.WithLabelValues(metric.Name).Inc()
				zap.L().Error("Metric with unknown type cannot get processed!",
					zap.String(fldQuery, entry.Query), zap.String(fldMetric, metric.Name),
					zap.String(fldType, metric.Type))
			}
		}
	}
}

// Preprocessing of a metric depending on given labels.
// The strategy defines how to proceed.
func applyMetricStrategy(entry *ConfigEntry, metric *MetricEntry, labels [][]string) {
	if len((*entry).Labels) < 1 {
		// ohne Labels
		applyMetricStrategyUnlabeled(metric, labels)
	} else {
		// mit Labels
		var newCollection LabelCollection
		for _, labelSet := range labels {
			newCollection = addLabelSet(newCollection, labelSet)
		}
		for _, labelSet := range (*metric).CurrentLabels {
			if !containsLabelSet(newCollection, labelSet) {
				switch (*metric).Strategy {
				case strategyDrop:
					deleteMetricWithLabels(metric, labelSet)
				case strategyZero:
					setMetricToZero(metric, labelSet)
				case strategyKeep:
					newCollection = addLabelSet(newCollection, labelSet)
					zap.L().Debug("Keep already existing value", zap.String(fldMetric, (*metric).Name),
						zap.Strings(fldLabels, labelSet))
				}
			}
		}
		(*metric).CurrentLabels = newCollection
	}
}

// Application of the strategy on a metric without labels
//
// Here we have only potential de-registration and re-registration as options,
// because we don't have label combinations to deal with.
func applyMetricStrategyUnlabeled(metric *MetricEntry, labels [][]string) {
	if len(labels) < 1 {
		applyStrategyForVanished(metric)
	} else {
		applyStrategyForReappeared(metric)
	}
}

// Application of the strategy on a metric without labels
// after no new values are available
func applyStrategyForVanished(metric *MetricEntry) {
	switch (*metric).Strategy {
	case strategyDrop:
		if (*metric).Registered {
			zap.L().Debug("De-registering metric", zap.String(fldMetric, (*metric).Name))
			switch (*metric).Type {
			case metricTypeGauge:
				prometheus.Unregister(Gauges[metric.Name])
			case metricTypeCounter:
				prometheus.Unregister(Counters[metric.Name])
			}
			(*metric).Registered = false
		}
	case strategyZero:
		zap.L().Debug(logMsgResetMetric, zap.String(fldMetric, (*metric).Name))
		labelSet := []string{}
		setMetricToZero(metric, labelSet)
	}
}

// Application of the strategy on a metric without labels
// after new values are available again
func applyStrategyForReappeared(metric *MetricEntry) {
	switch (*metric).Strategy {
	case strategyDrop:
		if !(*metric).Registered {
			zap.L().Debug("Re-registering metric again", zap.String(fldMetric, (*metric).Name))
			switch (*metric).Type {
			case metricTypeGauge:
				if err := prometheus.Register(Gauges[metric.Name]); err != nil {
					zap.L().Error("Re-registration failed", zap.String(fldGauge, metric.Name), zap.Error(err))
				}
			case metricTypeCounter:
				if err := prometheus.Register(Counters[metric.Name]); err != nil {
					zap.L().Error("Re-registration failed", zap.String(fldCounter, metric.Name), zap.Error(err))
				}
			}
			(*metric).Registered = true
		}
	}
}

// Set a metric for a given label combination to zero
func setMetricToZero(metric *MetricEntry, labels []string) {
	zap.L().Debug(logMsgResetMetric, zap.String(fldMetric, (*metric).Name), zap.Strings(fldLabels, labels))
	switch (*metric).Type {
	case metricTypeGauge:
		processGaugeMetric(metric.Name, 0.0, labels)
	case metricTypeCounter:
		processCounterMetric(metric.Name, 0.0, labels)
	}
}

// Remove the metric for a given label combination
func deleteMetricWithLabels(metric *MetricEntry, labels []string) {
	zap.L().Debug("Remove the metric for given labels",
		zap.String(fldMetric, (*metric).Name), zap.Strings(fldLabels, labels))
	switch (*metric).Type {
	case metricTypeGauge:
		GaugeVecs[metric.Name].DeleteLabelValues(labels...)
	case metricTypeCounter:
		CounterVecs[metric.Name].DeleteLabelValues(labels...)
	}
}

// Processing of a Gauge metric
func processGaugeMetric(gaugeName string, value float64, labels []string) {
	if len(labels) < 1 {
		// without labels
		Gauges[gaugeName].Set(value)
	} else {
		// with labels
		GaugeVecs[gaugeName].WithLabelValues(labels...).Set(value)
	}
}

// Processing of a Counter metric
func processCounterMetric(counterName string, value float64, labels []string) {
	if len(labels) < 1 {
		// without labels
		Counters[counterName].Add(value)
	} else {
		// with labels
		CounterVecs[counterName].WithLabelValues(labels...).Add(value)
	}
}
