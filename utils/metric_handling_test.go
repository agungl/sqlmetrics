package utils

import (
	"fmt"
	"testing"

	assertion "github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

var configStr = `
- query: multiple_metrics_with_labels_example
  metrics:
    - name: metric_m1
      type: gauge
      help: Metric 1
      strategy: drop
    - name: metric_m2
      type: gauge
      help: Metric 2
      strategy: zero
    - name: metric_m3
      type: counter
      help: Metric 3
      strategy: keep
    - name: metric_m4
      type: counter
      help: Metric 4
      strategy: drop
  labels:
    - label_1
    - label_2
  sql: SELECT 1, 2, 3, 4, 'Label1', 'Label2' FROM dual

- query: single_gauge_metric_without_label_drop_example
  metrics:
    - name: gauge_metric_1
      type: gauge
      help: Gauge Metric 1
      strategy: drop
  sql: SELECT 1, FROM dual

- query: single_counter_metric_without_label_drop_example
  metrics:
    - name: counter_metric_1
      type: counter
      help: Counter Metric 1
      strategy: drop
  sql: SELECT 1, FROM dual

- query: single_gauge_metric_without_label_zero_example
  metrics:
    - name: gauge_metric_2
      type: gauge
      help: Gauge Metric 2
      strategy: zero
  sql: SELECT 1, FROM dual

- query: single_counter_metric_without_zero_example
  metrics:
    - name: counter_metric_2
      type: counter
      help: Counter Metric 2
      strategy: zero
  sql: SELECT 1, FROM dual

- query: single_invalid_metric
  metrics:
    - name: counter_metric_3
      type: invalid
      help: Counter Metric 3
  sql: SELECT 1, FROM dual`

type DataFetcherMock struct {
	metricValues [][]float64
	labelValues  [][]string
}

func (dfm *DataFetcherMock) ReadValuesAndLabels(entry *ConfigEntry) ([][]float64, [][]string, error) {
	var err error = nil
	metricCount := len(entry.Metrics)
	labelCount := len(entry.Labels)
	for i := range dfm.metricValues {
		if metricCount != len(dfm.metricValues[i]) {
			err = fmt.Errorf("wrong number of metric values in provided data, expected=%d, found=%d",
				metricCount, len(dfm.metricValues[i]))
		}
	}
	if err == nil {
		for i := range dfm.labelValues {
			if labelCount != len(dfm.labelValues[i]) {
				err = fmt.Errorf("wrong number of label values in provided data, expected=%d, found=%d",
					labelCount, len(dfm.labelValues[i]))
			}
		}
	}
	if err != nil {
		panic(err)
	}
	return dfm.metricValues, dfm.labelValues, err
}

func (dfm *DataFetcherMock) ClearData() {
	dfm.metricValues = [][]float64{}
	dfm.labelValues = [][]string{}
}

func (dfm *DataFetcherMock) AddData(metrics []float64, labels []string) {
	dfm.metricValues = append(dfm.metricValues, metrics)
	dfm.labelValues = append(dfm.labelValues, labels)
}

func (dfm *DataFetcherMock) PrintData() {
	fmt.Printf("* metricValues: %v\n* labelValues : %v\n", dfm.metricValues, dfm.labelValues)
}

func TestProcessConfig(t *testing.T) {
	assert := assertion.New(t)

	configEntries := &ConfigEntries{}
	err := yaml.Unmarshal([]byte(configStr), configEntries)
	assert.Nil(err)
	assert.Equal(6, len(*configEntries))

	configEntries.initErrorCounters()
	for i := range *configEntries {
		RegisterMetrics(&((*configEntries)[i]))
	}
	var dfm DataFetcherMock

	// query: multiple_metrics_with_labels_example
	entry := (*configEntries)[0]
	assert.Equal(4, len(entry.Metrics))
	dfm.AddData([]float64{1.0, 1.0, 1.0, 1.0}, []string{"l1_1", "l2_1"})
	dfm.AddData([]float64{1.0, 1.0, 1.0, 1.0}, []string{"l1_1", "l2_2"})
	ProcessConfig(&dfm, &entry)
	assert.Equal(2, len(entry.Metrics[0].CurrentLabels))
	assert.Equal(2, len(entry.Metrics[1].CurrentLabels))
	assert.Equal(2, len(entry.Metrics[2].CurrentLabels))
	assert.Equal(2, len(entry.Metrics[3].CurrentLabels))
	dfm.ClearData()

	dfm.AddData([]float64{2.0, 2.0, 2.0, 2.0}, []string{"l1_1", "l2_1"})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	assert.True(entry.Metrics[1].Registered)
	assert.True(entry.Metrics[2].Registered)
	assert.True(entry.Metrics[3].Registered)
	assert.Equal(1, len(entry.Metrics[0].CurrentLabels), entry.Metrics[0].CurrentLabels)
	assert.Equal(1, len(entry.Metrics[1].CurrentLabels), entry.Metrics[1].CurrentLabels)
	assert.Equal(2, len(entry.Metrics[2].CurrentLabels), entry.Metrics[2].CurrentLabels)
	assert.Equal(1, len(entry.Metrics[3].CurrentLabels), entry.Metrics[3].CurrentLabels)
	dfm.ClearData()

	dfm.AddData([]float64{3.0, 3.0, 3.0, 3.0}, []string{"l1_1", "l2_1"})
	dfm.AddData([]float64{3.0, 3.0, 3.0, 3.0}, []string{"l1_1", "l2_2"})
	ProcessConfig(&dfm, &entry)
	assert.Equal(2, len(entry.Metrics[0].CurrentLabels))
	assert.Equal(2, len(entry.Metrics[1].CurrentLabels))
	assert.Equal(2, len(entry.Metrics[2].CurrentLabels))
	assert.Equal(2, len(entry.Metrics[3].CurrentLabels))
	dfm.ClearData()

	// query: single_counter_metrics_without_label_drop_example
	entry = (*configEntries)[1]
	dfm.AddData([]float64{1.0}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	ProcessConfig(&dfm, &entry)
	assert.False(entry.Metrics[0].Registered)
	dfm.AddData([]float64{1.5}, []string{})

	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	// query: single_counter_metrics_without_label_drop_example
	entry = (*configEntries)[2]
	dfm.AddData([]float64{1.0}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	ProcessConfig(&dfm, &entry)
	assert.False(entry.Metrics[0].Registered)

	dfm.AddData([]float64{2.0}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	// query: single_counter_metrics_without_label_zero_example
	entry = (*configEntries)[3]
	dfm.AddData([]float64{1.0}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)

	dfm.AddData([]float64{1.5}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	// query: single_counter_metrics_without_label_zero_example
	entry = (*configEntries)[4]
	dfm.AddData([]float64{1.0}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.AddData([]float64{2.0}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.True(entry.Metrics[0].Registered)
	dfm.ClearData()

	// query: single_invalid_metric
	entry = (*configEntries)[5]
	assert.False(entry.Metrics[0].Registered)
	dfm.AddData([]float64{1.0}, []string{})
	ProcessConfig(&dfm, &entry)
	assert.False(entry.Metrics[0].Registered)
}
