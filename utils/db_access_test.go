package utils

import (
	"context"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jackc/pgx/v5/stdlib"
	_ "github.com/microsoft/go-mssqldb"
	_ "github.com/sijms/go-ora/v2"
	assertion "github.com/stretchr/testify/assert"
)

func inGitlabCI() bool {
	_, present := os.LookupEnv("GITLAB_CI")
	return present
}

func dynamicHost(name string) string {
	if inGitlabCI() {
		return name
	}
	return "localhost"
}

func dynamicTnsDesc(description string) string {
	if inGitlabCI() {
		return description
	}
	return strings.Replace(description, "oracle", "localhost", 1)
}

// Oracle
const envValOraType = "oracle"
const envValOraHost = "oracle"
const envValOraPort = "1521"
const envValOraService = "FREEPDB1"
const envValOraSrvDef = "(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=oracle)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=FREEPDB1)))"
const envValOraUser = "system"
const envValOraPass = "oracle-18.4.0-XE"

func prepareTestOraDbConfig() DbConfig {
	dbCfg := DbConfig{
		Type: envValOraType,
		Host: dynamicHost(envValOraHost),
		Port: envValOraPort,
		Svce: envValOraService,
		User: envValOraUser,
		Pass: envValOraPass,
		Tout: 30,
	}
	return dbCfg
}

// PostgreSQL
const envValPgType = "postgres"
const envValPgHost = "postgres"
const envValPgPort = "5432"
const envValPgService = "postgres"
const envValPgUser = "postgres"
const envValPgPass = "example"

func prepareTestPgDbConfig() DbConfig {
	dbCfg := DbConfig{
		Type: envValPgType,
		Host: dynamicHost(envValPgHost),
		Port: envValPgPort,
		Svce: envValPgService,
		User: envValPgUser,
		Pass: envValPgPass,
	}
	return dbCfg
}

// MySQL
const envValMyType = "mysql"
const envValMyHost = "mysql"
const envValMyPort = "3306"
const envValMyService = "performance_schema"
const envValMyUser = "root"
const envValMyPass = "example"

func prepareTestMyDbConfig() DbConfig {
	dbCfg := DbConfig{
		Type: envValMyType,
		Host: dynamicHost(envValMyHost),
		Port: envValMyPort,
		Svce: envValMyService,
		User: envValMyUser,
		Pass: envValMyPass,
	}
	return dbCfg
}

// MsSQL
const envValMsType = "mssql"
const envValMsHost = "mssql"
const envValMsPort = "1433"
const envValMsService = "master"
const envValMsUser = "sa"
const envValMsPass = "L0ngexample"

func prepareTestMsDbConfig() DbConfig {
	dbCfg := DbConfig{
		Type: envValMsType,
		Host: dynamicHost(envValMsHost),
		Port: envValMsPort,
		Svce: envValMsService,
		User: envValMsUser,
		Pass: envValMsPass,
	}
	return dbCfg
}

func setTestEnvVars(t *testing.T) {
	assert := assertion.New(t)
	assert.Nil(os.Setenv(envDbType, envValOraType))
	assert.Nil(os.Setenv(envDbHost, envValOraHost))
	assert.Nil(os.Setenv(envDbPort, envValOraPort))
	assert.Nil(os.Setenv(envDbService, envValOraService))
	assert.Nil(os.Setenv(envDbSdef, envValOraSrvDef))
	assert.Nil(os.Setenv(envDbUser, envValOraUser))
	assert.Nil(os.Setenv(envDbPass, envValOraPass))
}

func unsetTestEnvVars(t *testing.T) {
	assert := assertion.New(t)
	assert.Nil(os.Unsetenv(envDbType))
	assert.Nil(os.Unsetenv(envDbHost))
	assert.Nil(os.Unsetenv(envDbPort))
	assert.Nil(os.Unsetenv(envDbService))
	assert.Nil(os.Unsetenv(envDbSdef))
	assert.Nil(os.Unsetenv(envDbUser))
	assert.Nil(os.Unsetenv(envDbPass))
}

func skipOraTests() bool {
	return len(os.Getenv("SKIP_ORA_TESTS")) > 0
}
func skipMsSQlTests() bool {
	return len(os.Getenv("SKIP_MSSQL_TESTS")) > 0
}

func TestDbConfig_ReadConfig_MissingConfig(t *testing.T) {
	assert := assertion.New(t)
	var dbCfg DbConfig
	assert.Error(dbCfg.readConfig("missing_db_config.yml"))
	assert.Empty(dbCfg.Type)
	assert.Empty(dbCfg.Host)
	assert.Empty(dbCfg.Port)
	assert.Empty(dbCfg.Svce)
	assert.Empty(dbCfg.Sdef)
	assert.Empty(dbCfg.User)
	assert.Empty(dbCfg.Pass)
}

func TestDbConfig_ReadConfig_InvalidConfig(t *testing.T) {
	assert := assertion.New(t)
	var dbCfg DbConfig
	assert.Error(dbCfg.readConfig("test_invalid_db_config.yml"))
	assert.Empty(dbCfg.Type)
	assert.Empty(dbCfg.Host)
	assert.Empty(dbCfg.Port)
	assert.Empty(dbCfg.Svce)
	assert.Empty(dbCfg.Sdef)
	assert.Empty(dbCfg.User)
	assert.Empty(dbCfg.Pass)
}

func TestDbConfig_ReadConfig_EmptyConfig(t *testing.T) {
	assert := assertion.New(t)
	var dbCfg DbConfig
	assert.Nil(dbCfg.readConfig("../db_config.yml"))
	assert.Empty(dbCfg.Type)
	assert.Empty(dbCfg.Host)
	assert.Empty(dbCfg.Port)
	assert.Empty(dbCfg.Svce)
	assert.Empty(dbCfg.Sdef)
	assert.Empty(dbCfg.User)
	assert.Empty(dbCfg.Pass)
}

func TestDbConfig_ReadEnvSettings_Unset(t *testing.T) {
	assert := assertion.New(t)
	var dbCfg DbConfig
	unsetTestEnvVars(t)
	dbCfg.readEnvSettings()
	assert.Empty(dbCfg.Type)
	assert.Empty(dbCfg.Host)
	assert.Empty(dbCfg.Port)
	assert.Empty(dbCfg.Svce)
	assert.Empty(dbCfg.Sdef)
	assert.Empty(dbCfg.User)
	assert.Empty(dbCfg.Pass)
}

func TestDbConfig_ReadEnvSettings(t *testing.T) {
	assert := assertion.New(t)
	setTestEnvVars(t)
	var dbCfg DbConfig
	dbCfg.readEnvSettings()
	assert.Equal(envValOraType, dbCfg.Type)
	assert.Equal(envValOraHost, dbCfg.Host)
	assert.Equal(envValOraPort, dbCfg.Port)
	assert.Equal(envValOraService, dbCfg.Svce)
	assert.Equal(envValOraSrvDef, dbCfg.Sdef)
	assert.Equal(envValOraUser, dbCfg.User)
	assert.Equal(envValOraPass, dbCfg.Pass)
}

func TestDbConfig_InitDbConfig(t *testing.T) {
	assert := assertion.New(t)
	setTestEnvVars(t)
	var dbCfg DbConfig
	dbCfg.InitDbConfig("missing_db_config.yml")
	assert.Equal(envValOraType, dbCfg.Type)
	assert.Equal(envValOraHost, dbCfg.Host)
	assert.Equal(envValOraPort, dbCfg.Port)
	assert.Equal(envValOraService, dbCfg.Svce)
	assert.Equal(envValOraSrvDef, dbCfg.Sdef)
	assert.Equal(envValOraUser, dbCfg.User)
	assert.Equal(envValOraPass, dbCfg.Pass)
}

func TestDbConfig_InitDbConfig_DefaultForType(t *testing.T) {
	assert := assertion.New(t)
	unsetTestEnvVars(t)
	var dbCfg DbConfig
	dbCfg.InitDbConfig("test_db_config_empty_type.yml")
	assert.Equal(envValOraType, dbCfg.Type)
	assert.Equal(envValOraHost, dbCfg.Host)
	assert.Equal(envValOraPort, dbCfg.Port)
	assert.Equal(envValOraService, dbCfg.Svce)
	assert.Equal(envValOraSrvDef, dbCfg.Sdef)
	assert.Equal(envValOraUser, dbCfg.User)
	assert.Equal(envValOraPass, dbCfg.Pass)
}

func TestDbConfig_InitDbConfig_FailingOnType(t *testing.T) {
	assert := assertion.New(t)
	unsetTestEnvVars(t)
	var dbCfg DbConfig
	assert.Panics(func() {
		dbCfg.InitDbConfig("test_db_config_wrong_type.yml")
	})
	assert.Equal("invalid", dbCfg.Type)
	assert.Equal(envValOraHost, dbCfg.Host)
	assert.Equal(envValOraPort, dbCfg.Port)
	assert.Equal(envValOraService, dbCfg.Svce)
	assert.Equal(envValOraSrvDef, dbCfg.Sdef)
	assert.Equal(envValOraUser, dbCfg.User)
	assert.Equal(envValOraPass, dbCfg.Pass)
}

func TestConnectOra_Success(t *testing.T) {
	if skipOraTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestOraDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()
	assert.Nil(db.Close())
}

func TestConnectOraViaTNS_Success(t *testing.T) {
	if skipOraTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestOraDbConfig()
	dbCfg.Sdef = dynamicTnsDesc(envValOraSrvDef)

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()
	assert.Nil(db.Close())
}

func TestConnectOra_Fail(t *testing.T) {
	if skipOraTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestOraDbConfig()
	dbCfg.Port = "1522"

	db, err := Connect(dbCfg)
	assert.NotNil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.NotNil(err)
	cancel()
}

func TestConnectPg_Success(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestPgDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()
	assert.Nil(db.Close())
}

func TestConnectPg_Fail(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestPgDbConfig()
	dbCfg.Port = "5433"

	db, err := Connect(dbCfg)
	assert.NotNil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.NotNil(err)
	cancel()
}

func TestConnectMs_Success(t *testing.T) {
	if skipMsSQlTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestMsDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()
	assert.Nil(db.Close())
}

func TestConnectMs_Fail(t *testing.T) {
	if skipMsSQlTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestMsDbConfig()
	dbCfg.Port = "1434"

	db, err := Connect(dbCfg)
	assert.NotNil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.NotNil(err)
	cancel()
}

func TestConnectMy_Success(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestMyDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()
	assert.Nil(db.Close())
}

func TestConnectMy_Fail(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestMyDbConfig()
	dbCfg.Port = "3307"

	db, err := Connect(dbCfg)
	assert.NotNil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.NotNil(err)
	cancel()
}

func TestReadValuesAndLabelsBySql_PgSuccess(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestPgDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	values, labels, err := readValuesAndLabelsBySql(db, "SELECT 0, VERSION()", 1, 1)
	assert.Nil(err)
	assert.Equal(1, len(values))
	assert.Equal(1, len(values[0]))
	assert.Equal(1, len(labels))
	assert.Equal(1, len(labels[0]))
	assert.Equal(0.0, values[0][0])
	assert.Equal("PostgreSQL 9.6.24 on x86_64-pc-linux-gnu (Debian 9.6.24-1.pgdg90+1), compiled by gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516, 64-bit", labels[0][0])
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_PgInvalid(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestPgDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	sql := "SELECT 0, VERSION() FROM dual"
	errStr := "ERROR: relation \"dual\" does not exist (SQLSTATE 42P01)"
	values, labels, err := readValuesAndLabelsBySql(db, sql, 1, 1)
	assert.NotNil(err)
	assert.EqualError(err, fmt.Sprintf("error during the preparation [%s]: %v", sql, errStr))
	assert.Equal(0, len(values))
	assert.Equal(0, len(labels))
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_MySuccess(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestMyDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	values, labels, err := readValuesAndLabelsBySql(db, "SELECT 0, VERSION()", 1, 1)
	assert.Nil(err)
	assert.Equal(1, len(values))
	assert.Equal(1, len(values[0]))
	assert.Equal(1, len(labels))
	assert.Equal(1, len(labels[0]))
	assert.Equal(0.0, values[0][0])
	assert.Equal("9.2.0", labels[0][0])
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_MyInvalid(t *testing.T) {
	assert := assertion.New(t)
	dbCfg := prepareTestMyDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	sql := "SELECT 0, VERSION() FROM mysql.dummy"
	errStr := "Error 1146 (42S02): Table 'mysql.dummy' doesn't exist"
	values, labels, err := readValuesAndLabelsBySql(db, sql, 1, 1)
	assert.NotNil(err)
	assert.EqualError(err, fmt.Sprintf("error during the preparation [%s]: %v", sql, errStr))
	assert.Equal(0, len(values))
	assert.Equal(0, len(labels))
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_MsSuccess(t *testing.T) {
	if skipMsSQlTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestMsDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	values, labels, err := readValuesAndLabelsBySql(db, "SELECT 0, SERVERPROPERTY('productversion')", 1, 1)
	assert.Nil(err)
	assert.Equal(1, len(values))
	assert.Equal(1, len(values[0]))
	assert.Equal(1, len(labels))
	assert.Equal(1, len(labels[0]))
	assert.Equal(0.0, values[0][0])
	assert.Equal("16.0.4175.1", labels[0][0])
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_MsInvalid(t *testing.T) {
	if skipMsSQlTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestMsDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	sql := "SELECT 0, @@version FROM dummy"
	errStr := "mssql: Invalid object name 'dummy'."
	values, labels, err := readValuesAndLabelsBySql(db, sql, 1, 1)
	assert.NotNil(err)
	assert.EqualError(err, fmt.Sprintf("error during the execution [%s]: %v", sql, errStr))
	assert.Equal(0, len(values))
	assert.Equal(0, len(labels))
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_OraSuccess(t *testing.T) {
	if skipOraTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestOraDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	values, labels, err := readValuesAndLabelsBySql(db, "SELECT 0, banner FROM v$version WHERE rownum=1", 1, 1)
	assert.Nil(err)
	assert.Equal(1, len(values))
	assert.Equal(1, len(values[0]))
	assert.Equal(1, len(labels))
	assert.Equal(1, len(labels[0]))
	assert.Equal(0.0, values[0][0])
	assert.Equal("Oracle Database 23ai Free Release 23.0.0.0.0 - Develop, Learn, and Run for Free", labels[0][0])
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_OraMismatch(t *testing.T) {
	if skipOraTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestOraDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	values, labels, err := readValuesAndLabelsBySql(db, "SELECT 0, banner FROM v$version WHERE rownum=1", 1, 2)
	assert.NotNil(err)
	assert.EqualError(err, "the column count in SQL (2) does not meet the expectation (1 + 2)")
	assert.Equal(0, len(values))
	assert.Equal(0, len(labels))
	assert.Nil(db.Close())
}

func TestReadValuesAndLabelsBySql_OraInvalid(t *testing.T) {
	if skipOraTests() {
		return
	}
	assert := assertion.New(t)
	dbCfg := prepareTestOraDbConfig()

	db, err := Connect(dbCfg)
	assert.Nil(err)
	assert.NotNil(db)
	bgContext := context.Background()
	ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
	err = db.PingContext(ctx)
	assert.Nil(err)
	cancel()

	sql := "SELECT 0, banner FROM dual WHERE rownum=1"
	errStr := "ORA-00904: \"BANNER\": invalid identifier\n"
	values, labels, err := readValuesAndLabelsBySql(db, sql, 1, 1)
	assert.NotNil(err)
	assert.EqualError(err, fmt.Sprintf("error during the execution [%s]: %v", sql, errStr))
	assert.Equal(0, len(values))
	assert.Equal(0, len(labels))
	assert.Nil(db.Close())
}
