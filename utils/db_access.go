package utils

import (
	"context"
	"database/sql"
	"fmt"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	goOra "github.com/sijms/go-ora/v2"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

// constants for the names of the environment variables:
const envDbService = "DB_SERVICE"
const envDbName = "DB_NAME"
const envDbType = "DB_TYPE"
const envDbHost = "DB_HOST"
const envDbPort = "DB_PORT"
const envDbUser = "DB_USER"
const envDbPass = "DB_PASS"
const envDbSdef = "DB_TNS"

// DbConfig is a structure for the database configuration
type DbConfig struct {
	Type string `yaml:"type"`
	Name string `yaml:"name"`
	Host string `yaml:"host"`
	Port string `yaml:"port"`
	Svce string `yaml:"service"`
	User string `yaml:"user"`
	Pass string `yaml:"password"`
	Sdef string `yaml:"tns"`
	Tout int    `yaml:"timeout"`
}

// supported DB drivers
const dbTypeOracle = "oracle"
const dbTypePostgres = "postgres"
const dbTypeMySql = "mysql"
const dbTypeMsSql = "mssql"

var supportedDbTypes = []string{dbTypeOracle, dbTypePostgres, dbTypeMySql}

// InitDbConfig reads the DB connection parameters from a config file
// and considers environment variables if set
func (dbCfg *DbConfig) InitDbConfig(configFileName string) {
	if err := dbCfg.readConfig(configFileName); err != nil {
		zap.L().Error("Error while reading the DB access configuration", zap.String(fldFilename, configFileName), zap.Error(err))
	}
	dbCfg.readEnvSettings()
	if len(dbCfg.Type) == 0 {
		dbCfg.Type = dbTypeOracle
	}
	zap.L().Info("DB configuration", zap.String(envDbType, dbCfg.Type), zap.String(envDbName, dbCfg.Name),
		zap.String(envDbService, dbCfg.Svce), zap.String(envDbHost, dbCfg.Host),
		zap.String(envDbPort, dbCfg.Port), zap.String(envDbUser, dbCfg.User), zap.String(envDbPass, "*****"),
		zap.String(envDbSdef, dbCfg.Sdef))
	// check drivers for support and validity
	found := false
	for _, name := range supportedDbTypes {
		if name == dbCfg.Type {
			found = true
		}
	}
	if !found {
		zap.L().Panic("DB type is not supported",
			zap.String(envDbType, dbCfg.Type),
			zap.Strings("allowed", supportedDbTypes))
	}
}

// function to overwrite DB parameters with values from environment variables
func (dbCfg *DbConfig) readEnvSettings() {
	if os.Getenv(envDbService) != "" {
		dbCfg.Svce = os.Getenv(envDbService)
	}
	if os.Getenv(envDbType) != "" {
		dbCfg.Type = os.Getenv(envDbType)
	}
	if os.Getenv(envDbName) != "" {
		dbCfg.Name = os.Getenv(envDbName)
	}
	if os.Getenv(envDbHost) != "" {
		dbCfg.Host = os.Getenv(envDbHost)
	}
	if os.Getenv(envDbPort) != "" {
		dbCfg.Port = os.Getenv(envDbPort)
	}
	if os.Getenv(envDbUser) != "" {
		dbCfg.User = os.Getenv(envDbUser)
	}
	if os.Getenv(envDbPass) != "" {
		dbCfg.Pass = os.Getenv(envDbPass)
	}
	if os.Getenv(envDbSdef) != "" {
		dbCfg.Sdef = os.Getenv(envDbSdef)
	}
}

// function for reading the DB connection parameters from a config file
func (dbCfg *DbConfig) readConfig(configFileName string) error {
	const errMsg = "cannot read DB configuration: %v"
	if configFileName != "" {
		if _, err := os.Stat(configFileName); os.IsNotExist(err) {
			zap.L().Error("The file does not exist! No DB configuration available!",
				zap.String(fldFilename, configFileName))
			return fmt.Errorf(errMsg, err)
		} else {
			zap.L().Info("Reading the DB configuration from file", zap.String(fldFilename, configFileName))
			yamlFile, err := os.ReadFile(configFileName)
			if err != nil {
				zap.L().Error("Error while reading from the file",
					zap.String(fldFilename, configFileName),
					zap.Error(err))
				return fmt.Errorf(errMsg, err)
			} else {
				err = yaml.Unmarshal(yamlFile, dbCfg)
				if err != nil {
					zap.L().Error("Error while the evaluation of the file contents", zap.Error(err))
					return fmt.Errorf(errMsg, err)
				}
			}
		}
	}
	return nil
}

// function to determine the driver name depending on the DB type
func (dbCfg *DbConfig) getDriverName() string {
	driverName := "unknown"
	switch dbCfg.Type {
	case dbTypeOracle:
		driverName = "oracle"
	case dbTypePostgres:
		driverName = "pgx"
	case dbTypeMySql:
		driverName = "mysql"
	case dbTypeMsSql:
		driverName = "sqlserver"
	}
	return driverName
}

// function to create the connection string depending on the DB type
//
// If an entry in TNS format is provided, it overrules the values
// for host, port and service.
func (dbCfg *DbConfig) buildConnectionStr() (connStr string) {
	switch dbCfg.Type {
	case dbTypeOracle:
		var params = map[string]string{}
		connStr = fmt.Sprintf("oracle://%s:%s@%s:%s/%s", dbCfg.User, url.QueryEscape(dbCfg.Pass), dbCfg.Host, dbCfg.Port, dbCfg.Svce)
		if dbCfg.Tout > 0 {
			connStr = fmt.Sprintf("%s?%s=%d", connStr, url.QueryEscape("connection timeout"), dbCfg.Tout)
			params["connection timeout"] = strconv.Itoa(dbCfg.Tout)
		}
		if len(dbCfg.Sdef) > 0 {
			connStr = goOra.BuildJDBC(dbCfg.User, dbCfg.Pass, dbCfg.Sdef, params)
		}
	case dbTypePostgres:
		connStr = fmt.Sprintf("postgresql://%s:%s@%s:%s/%s", dbCfg.User, url.QueryEscape(dbCfg.Pass), dbCfg.Host, dbCfg.Port, dbCfg.Name)
	case dbTypeMySql:
		connStr = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbCfg.User, url.QueryEscape(dbCfg.Pass), dbCfg.Host, dbCfg.Port, dbCfg.Name)
	case dbTypeMsSql:
		connStr = fmt.Sprintf("sqlserver://%s:%s@%s:%s?database=%s&connection+timeout=30", dbCfg.User, url.QueryEscape(dbCfg.Pass), dbCfg.Host, dbCfg.Port, dbCfg.Name)
	}
	return
}

// Connect establishes a DB connection including an initial check
//
// If the DB cannot get opened, the corresponding error is returned
// and then shown in the log.
func Connect(dbCfg DbConfig) (*sql.DB, error) {
	connStr := dbCfg.buildConnectionStr()
	driverName := dbCfg.getDriverName()
	zap.L().Debug("DB login starts",
		zap.String("connStr", strings.Replace(connStr, url.QueryEscape(dbCfg.Pass), "*****", 1)),
		zap.String("driver", driverName))
	db, err := sql.Open(driverName, connStr)
	if err == nil {
		bgContext := context.Background()
		ctx, cancel := context.WithTimeout(bgContext, 3*time.Second)
		defer cancel()
		err = db.PingContext(ctx)
	}
	return db, err
}

// Reading of n floating point values and m strings as result of a SQL query with multiple lines
//
// Example: SELECT count(*), sum(abc), name, firstname FROM xyz GROUP BY name, firstname
//
// The parameters are a DB instance, a SQL statement, and the quantities of the metrics and labels.
// The SQL has to retrieve exactly as many numeric values as the metrics, and then as many
// strings as the labels.
// The column count of the SQL must match the quantities of the metrics and labels.
// Otherwise, runtime errors will occur.
//
// If an error occurs with in the data retrieval, the error is propagated.
// The result must be considered as invalid in that case.
func readValuesAndLabelsBySql(db *sql.DB, sql string, metricCount int, labelCount int) (values [][]float64, labels [][]string, err error) {
	flatSql := strings.Replace(sql, "\n", "", -1)
	stmt, prepareError := db.Prepare(sql)
	if prepareError != nil {
		return values, labels, fmt.Errorf("error during the preparation [%s]: %v", flatSql, prepareError)
	}
	defer func() {
		if stmtCloseError := stmt.Close(); stmtCloseError != nil {
			zap.L().Error("Error during closing of the SQL statement", zap.Error(stmtCloseError))
		}
	}()
	rows, queryError := stmt.Query()
	if queryError != nil {
		return values, labels, fmt.Errorf("error during the execution [%s]: %v", flatSql, queryError)
	}
	defer func() {
		if err := rows.Close(); err != nil {
			zap.L().Error("Error during closing of the SQL result set", zap.Error(err))
		}
	}()
	cols, columnsError := rows.Columns()
	if columnsError != nil {
		return values, labels, fmt.Errorf("error during the determination of the result columns: %v", columnsError)
	} else {
		return processRowSet(rows, cols, flatSql, metricCount, labelCount)
	}
}

// Processing of the result records of the SQL statements and mapping the
// values into arrays for the metric values and corresponding label values
func processRowSet(rows *sql.Rows, cols []string, flatSql string, metricCount int, labelCount int) (values [][]float64, labels [][]string, err error) {
	if (metricCount + labelCount) != len(cols) {
		return values, labels, fmt.Errorf("the column count in SQL (%d) does not meet the expectation (%d + %d)", len(cols), metricCount, labelCount)
	}
	for rows.Next() {
		// We cannot directly read the result variables.
		// Instead, we need an intermediate structure with pointers
		// to the real target variables, because we cannot assign
		// the dedicated column count in the source code.
		rowValues := make([]float64, metricCount)
		rowLabels := make([]string, labelCount)
		dest := make([]interface{}, len(cols))
		// Pointers in interface slice are set to real target position
		for i := range rowValues {
			dest[i] = &rowValues[i]
		}
		for j := range rowLabels {
			dest[j+metricCount] = &rowLabels[j]
		}
		// Read data from the current result record
		if err := rows.Scan(dest...); err != nil {
			return values, labels, fmt.Errorf("error while reading data [%s]: %v", flatSql, err)
		} else {
			values = append(values, rowValues)
			labels = append(labels, rowLabels)
		}
	}
	if err := rows.Err(); err != nil {
		return values, labels, fmt.Errorf("error during the processing of the result [%s]: %v", flatSql, err)
	}
	return
}
