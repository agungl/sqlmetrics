package utils

import (
	"database/sql"

	"go.uber.org/zap"
)

// DataFetcher defines an interface for implementations which take over
// the data retrieval for the metrics defined in the ConfigEntry.
//
// The result is provided as outer arrays which represents the data sets,
// and an inner array which represents the metrics and labels build
// on top of the data in the set.
// Both outer arrays must have the same size. The size of the inner arrays
// corresponds with the metric count and label count, which have been
// defined before.
// In case of an error, the data has to be considered as invalid.
type DataFetcher interface {
	ReadValuesAndLabels(entry *ConfigEntry) ([][]float64, [][]string, error)
}

// SqlDataFetcher provides the retrieval of the metric data from a SQL database
// and implements the DataFetcher interface.
//
// The element DbInstance is a reference to a sql.DB object.
type SqlDataFetcher struct {
	DbInstance *sql.DB
}

// ReadValuesAndLabels implements the interpretation of the result of a query
// into metrics (optionally with labels) as well as the logging depending on the
// result.
// Further a MetricBucket is created which can optionally be used in the
// next processing steps.
func (sdf SqlDataFetcher) ReadValuesAndLabels(entry *ConfigEntry) ([][]float64, [][]string, error) {
	values, labels, err := readValuesAndLabelsBySql(sdf.DbInstance, entry.Sql, len(entry.Metrics), len(entry.Labels))
	if err != nil {
		for _, metric := range entry.Metrics {
			retrievalErrors.WithLabelValues(metric.Name).Inc()
			zap.L().Warn(logMsgDBAccessError, zap.String(fldMetric, metric.Name), zap.Error(err))
		}
	} else {
		for i := 0; i < len(values); i++ {
			for j, metric := range entry.Metrics {
				logFieldName := fldGauge
				if metric.Type == metricTypeCounter {
					logFieldName = fldCounter
				}
				zap.L().Debug(logMsgDBAccessResult,
					zap.String(logFieldName, metric.Name),
					zap.String(fldQuery, entry.Query),
					zap.Float64(fldValue, values[i][j]),
					zap.Strings(fldLabels, labels[i]))
			}
		}
	}
	return values, labels, err
}
