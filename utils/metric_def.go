package utils

import (
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

// constants for the field names in the log
const fldFilename = "filename"
const fldQuery = "query"
const fldMetric = "metric"
const fldType = "type"
const fldGauge = "gauge"
const fldCounter = "counter"
const fldValue = "value"
const fldLabels = "labels"
const fldStrategy = "strategy"

// constants for the metric types
const metricTypeCounter = "counter"
const metricTypeGauge = "gauge"

// constants for the metric strategies
const strategyKeep = "keep"
const strategyZero = "zero"
const strategyDrop = "drop"

// messages in the log
const logMsgDBAccessError = "Error while accessing the database"
const logMsgDBAccessResult = "Result set returned from the DB"
const logMsgResetMetric = "Reset metric value to 0 (zero)"

// LabelCollection is used to store all used label value combinations
// of a metric
type LabelCollection [][]string

// MetricEntry describes the mapping of the column value in a result set to a metric.
// Labels are optional and described later. Then they are valid for all metrics of a ConfigEntry.
// The MetricEntries have to be in the same order as the corresponding columns in the result set.
//
// The Strategy defines how to handle metrics if in the result set of a query is no record at all.
// The default strategy is *keep*. The least recently read value for the metric is kept until
// a new value is read from the DB.
// The strategy *zero* sets the value of a metric to zero, if no data is available in a query.
// To delete metrics if no data is found in the database, the strategy *drop* can be used.
//
// CurrentLabels and Registered are relevant only at runtime.
// CurrentLabels then stores any currently valid label combinations for the metric.
// Registered defines if the metric is currently registered resp. active.
type MetricEntry struct {
	Name          string `yaml:"name"`
	Type          string `yaml:"type"`
	Help          string `yaml:"help"`
	Strategy      string `yaml:"strategy"`
	CurrentLabels LabelCollection
	Registered    bool
}

// ConfigEntry is the structure for the configuration entries
// which can be read (in YAML format) from a config file.
//
// Name is kept for backward compatibility.
// Query is a comment which has no influence on the processing.
// ReadCount is a counter which should be increased on each retrieval of the configuration values.
type ConfigEntry struct {
	Name      string        `yaml:"name"`
	Query     string        `yaml:"query"`
	Metrics   []MetricEntry `yaml:"metrics"`
	Labels    []string      `yaml:"labels"`
	Sql       string        `yaml:"sql"`
	Sleep     int           `yaml:"sleep"`
	ReadCount int
}

// ConfigEntries is a type for an array of configuration entries
// which as the whole define the configuration of the application.
type ConfigEntries []ConfigEntry

// Counters and Gauges are maps for the configured counter and gauge metrics,
// the key is based on the name of the corresponding metric
var Counters map[string]prometheus.Counter
var Gauges map[string]prometheus.Gauge

// CounterVecs and GaugeVecs are maps for the configured labeled counters and gauges,
// the key is based on the name of the corresponding metric
var CounterVecs map[string]*prometheus.CounterVec
var GaugeVecs map[string]*prometheus.GaugeVec

// Internal metric for errors during the DB access (per declared external metric)
var retrievalErrors *prometheus.GaugeVec

// Internal metric for the duration of the DB access (per declared external metric)
var sqlDurations *prometheus.SummaryVec

// initialization of the module: create empty global maps for the metrics
func init() {
	Counters = make(map[string]prometheus.Counter)
	Gauges = make(map[string]prometheus.Gauge)
	CounterVecs = make(map[string]*prometheus.CounterVec)
	GaugeVecs = make(map[string]*prometheus.GaugeVec)

	retrievalErrors = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "metrics_provider_metric_retrieval_errors",
			Help: "Error count when reading metric data from the DB",
		},
		[]string{"metric_name"},
	)
	prometheus.MustRegister(retrievalErrors)

	sqlDurations = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name:       "metrics_provider_sql_durations_seconds",
			Help:       "SQL query duration in seconds when reading metric data from the DB",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"query_name"},
	)
	prometheus.MustRegister(sqlDurations)
}

// ReadConfig handles the reading of the metric configuration from a config file.
//
// The file is specified via the environment variable CONFIG. If it is not set,
// there will be a fall back to the value of the command line parameter -metric-config
// or eventually to the default name config.yml.
//
// If the config file cannot be found, or if an error occurs during the
// evaluation of the data, then an unchanged and empty configuration is produced.
func (c *ConfigEntries) ReadConfig(configFileName string) *ConfigEntries {
	cgfFileName := os.Getenv("CONFIG")
	if cgfFileName == "" {
		// get the file name from the command line parameter
		cgfFileName = configFileName
		if cgfFileName == "" {
			// Default file name if nothing has been specified
			cgfFileName = "config.yml"
		}
	}
	if _, err := os.Stat(cgfFileName); os.IsNotExist(err) {
		zap.L().Error("The file does not exist! No metric configuration available!", zap.String(fldFilename, cgfFileName))
	} else {
		zap.L().Info("Reading the metric configuration from file", zap.String(fldFilename, cgfFileName))
		yamlFileContents, err := os.ReadFile(cgfFileName)
		if err != nil {
			zap.L().Error("Error while reading from the file", zap.String(fldFilename, cgfFileName), zap.Error(err))
		} else {
			err = yaml.Unmarshal(yamlFileContents, c)
			if err != nil {
				zap.L().Error("Error while the evaluation of the file contents", zap.Error(err))
			}
		}
	}
	c.initErrorCounters()
	return c
}

// Set all error counters initially to zero and correct the strategy if needed
// before the first usage of the metrics.
func (c *ConfigEntries) initErrorCounters() {
	for _, entry := range *c {
		for i, metric := range entry.Metrics {
			retrievalErrors.WithLabelValues(metric.Name).Set(0.0)
			validateMetric(&(entry.Metrics[i]))
		}
	}
}

// Validation of the strategy for a metric
//
// A valid strategy must be defined. If the is nothing preset,
// or if the value in the config file is invalid, then a fallback to the
// strategy "keep" will be made, which was the only strategy before
// offering alternative strategies.
func validateMetric(metric *MetricEntry) {
	switch metric.Strategy {
	case strategyKeep:
	case strategyZero:
	case strategyDrop:
	default:
		if len(metric.Strategy) > 0 {
			zap.L().Warn("Invalid strategy defined, allowed are (keep, zero, drop)",
				zap.String(fldMetric, metric.Name),
				zap.String(fldStrategy, metric.Strategy))
		}
		metric.Strategy = strategyKeep
	}
}
