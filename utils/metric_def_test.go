package utils

import (
	"testing"

	assertion "github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

func TestConfigEntries_ReadConfig_MissingConfig(t *testing.T) {
	assert := assertion.New(t)
	configEntries := &ConfigEntries{}
	configEntries.ReadConfig("missing_config.yml")
	assert.Equal(0, len(*configEntries))
}

func TestConfigEntries_ReadConfig_InvalidConfig(t *testing.T) {
	assert := assertion.New(t)
	configEntries := &ConfigEntries{}
	configEntries.ReadConfig("test_invalid_config.yml")
	assert.Equal(0, len(*configEntries))
}

func TestConfigEntries_ReadConfig_EmptyConfig(t *testing.T) {
	assert := assertion.New(t)
	configEntries := &ConfigEntries{}
	configEntries.ReadConfig("../test_config.yml")
	assert.Equal(0, len(*configEntries))
}

func TestConfigEntries_ReadConfig_TestConfig(t *testing.T) {
	assert := assertion.New(t)
	configEntries := &ConfigEntries{}
	configEntries.ReadConfig("test_config.yml")
	assert.Equal(2, len(*configEntries))

	entry := (*configEntries)[0]
	assert.Equal("pg_uptime", entry.Query)
	assert.Equal(1, len(entry.Metrics))
	assert.Equal(0, len(entry.Labels))
	assert.Equal(120, entry.Sleep)
	metric := entry.Metrics[0]
	assert.Equal("pg_uptime", metric.Name)
	assert.Equal("gauge", metric.Type)
	assert.Equal("Uptime in seconds", metric.Help)
	assert.Equal("keep", metric.Strategy)
	assert.Equal(0, len(metric.CurrentLabels))

	entry = (*configEntries)[1]
	assert.Equal(1, len(entry.Labels))
	assert.Equal("version", entry.Labels[0])
	sql := "SELECT 0, VERSION()\n"
	assert.Equal(sql, entry.Sql)
}

func TestMultiMetricsAndLabels(t *testing.T) {
	assert := assertion.New(t)
	configStr := `
- query: multi_example
  metrics:
    - name: metric_1
      type: gauge
      help: Metric 1
      strategy: drop
    - name: metric_2
      type: counter
      help: Metric 2
      strategy: zero
    - name: metric_3
      type: gauge
      help: Metric 3
      strategy: unknown
    - name: metric_4
      type: gauge
      help: Metric 4
      strategy: keep
  labels:
    - label_1
    - label_2
  sql: SELECT 1, 2, 3, 4, 'Label1', 'Label2' FROM dual`

	configEntries := &ConfigEntries{}
	err := yaml.Unmarshal([]byte(configStr), configEntries)
	assert.Nil(err)
	assert.Equal(1, len(*configEntries))

	configEntries.initErrorCounters()
	entry := (*configEntries)[0]
	assert.Equal(4, len(entry.Metrics))
	assert.Equal(2, len(entry.Labels))
	assert.Equal("gauge", entry.Metrics[0].Type)
	assert.Equal("drop", entry.Metrics[0].Strategy)
	assert.Equal("counter", entry.Metrics[1].Type)
	assert.Equal("zero", entry.Metrics[1].Strategy)
	assert.Equal("keep", entry.Metrics[2].Strategy)
	assert.Equal("keep", entry.Metrics[3].Strategy)
}
