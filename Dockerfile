FROM golang:1.24.0 AS compile_image

# use non-root user with uid=1000 which fits most distributions when providing a local volume during development
RUN mkdir -p /opt/go && \
    useradd -g users --home-dir /opt/go --no-create-home --no-user-group -u 1000 developer && \
    chown -R developer:users /opt/go
USER developer

# copy project sources and start the build
COPY --chown=1000:users ./*.go ./go.mod ./go.sum /opt/go/src/gitlab.com/agungl/sqlmetrics/
COPY --chown=1000:users ./utils/*.go /opt/go/src/gitlab.com/agungl/sqlmetrics/utils/
COPY --chown=1000:users ./.git/ /opt/go/src/gitlab.com/agungl/sqlmetrics/.git/
WORKDIR /opt/go/src/gitlab.com/agungl/sqlmetrics/
SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
RUN git config --global --add safe.directory /opt/go/src/gitlab.com/agungl/sqlmetrics && \
    go get -v -d && \
    CGO_ENABLED=0 go build -o /opt/go/sqlmetrics \
    -ldflags "-X main.version=$(git tag --sort=-version:refname | head -n 1) -X main.buildstamp=$(date -u '+%Y-%m-%d_%I:%M:%S%p') -X main.githash=$(git rev-parse HEAD)" \
    .

###########################################################

FROM oraclelinux:9-slim-fips
LABEL org.opencontainers.image.authors="a.gungl@gmx.de"

RUN microdnf -y upgrade && microdnf clean all

# settings
EXPOSE 8080
WORKDIR /opt/go

# copy binary
COPY --from=compile_image /opt/go/sqlmetrics /opt/go/
COPY ./config.yml /opt/go/

# use non-root user with uid=1000 which fits most distributions when providing a local volume during development
RUN useradd -G users metricuser && chown -R metricuser:users /opt/go
USER metricuser

HEALTHCHECK --start-period=5s CMD curl --fail -I http://localhost:8080/health || exit 1

# start the Metrics Provider
CMD ["/opt/go/sqlmetrics"]
