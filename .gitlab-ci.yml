# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
image: alpine:3.21
variables:
  GLR_IMAGE_TAG: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}"
  GLR_IMAGE_TAG_VERSION: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
  GLR_IMAGE_TAG_LATEST: "${CI_REGISTRY_IMAGE}:latest"
  ARTIFACT_FOLDER: "${CI_PROJECT_DIR}"
  TRIVY_CACHE: "${CI_PROJECT_DIR}/.cache"
  MODULE_NAME: "gitlab.com/agungl/sqlmetrics"

stages:
- source_scan
- test
- image_build
- image_scan
- report

hado_lint:
  stage: source_scan
  except:
  - schedules
  after_script:
  - cat ${ARTIFACT_FOLDER}/hadolint_results.txt
  script:
  - export HADOLINT_VERSION=$(wget -q -O - https://api.github.com/repos/hadolint/hadolint/releases/latest | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
  - wget https://github.com/hadolint/hadolint/releases/download/v${HADOLINT_VERSION}/hadolint-Linux-x86_64 && chmod +x hadolint-Linux-x86_64
  - ./hadolint-Linux-x86_64 --no-fail --style DL3033 Dockerfile > ${ARTIFACT_FOLDER}/hadolint_results.txt
  - ./hadolint-Linux-x86_64 --no-fail --style DL3033 -f json Dockerfile > ${ARTIFACT_FOLDER}/hadolint_results.json
  - ./hadolint-Linux-x86_64 --no-fail --style DL3033 -f gitlab_codeclimate Dockerfile > ${ARTIFACT_FOLDER}/gl_hadolint_results.json
  artifacts:
    when: always
    paths:
    - "$ARTIFACT_FOLDER/hadolint_results.txt"
    - "$ARTIFACT_FOLDER/hadolint_results.json"
    reports:
      codequality: "${ARTIFACT_FOLDER}/gl_hadolint_results.json"

source_scan_trivy:
  stage: source_scan
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  except:
  - schedules
  variables:
    CACHE_OPTIONS: "--cache-dir ${TRIVY_CACHE} --skip-dirs .cache"
  after_script:
  - cat ${ARTIFACT_FOLDER}/trivy_source_results.txt
  script:
  - trivy ${CACHE_OPTIONS} filesystem --no-progress --output ${ARTIFACT_FOLDER}/trivy_source_results.txt ${CI_PROJECT_DIR}
  - trivy ${CACHE_OPTIONS} filesystem --no-progress --exit-code 1 --severity CRITICAL -f json
    --output ${ARTIFACT_FOLDER}/trivy_source_results.json ${CI_PROJECT_DIR}
  artifacts:
    when: always
    paths:
    - "${ARTIFACT_FOLDER}/trivy_source_results.txt"
    - "${ARTIFACT_FOLDER}/trivy_source_results.json"
  cache:
    paths:
    - ".cache"

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml
- template: Security/Secret-Detection.gitlab-ci.yml

gotest:
  stage: test
  coverage: '/coverage: \d+.\d+% of statements/'
  image: golang:1.24.0
  except:
  - schedules
  services:
  - postgres:9.6.24
  - mysql:9.2.0
  - name: mcr.microsoft.com/mssql/server:2022-latest
    alias: mssql
  variables:
    ORACLE_PASSWORD: oracle-18.4.0-XE
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: example
    MYSQL_ROOT_PASSWORD: example
    MSSQL_SA_PASSWORD: L0ngexample
    ACCEPT_EULA: Y
    GODEBUG: x509negativeserial=1
  script:
  - apt update && apt -y upgrade && apt -y install sudo
  - cd ${CI_PROJECT_DIR}
  - export GOPATH=${CI_PROJECT_DIR}/.cache
  - export SKIP_ORA_TESTS=1
  - go mod tidy
  - go install gotest.tools/gotestsum@latest
  - $GOPATH/bin/gotestsum --junitfile ${CI_PROJECT_DIR}/test-report.xml --format testname ${MODULE_NAME} ${MODULE_NAME}/utils
  - go install github.com/boumenot/gocover-cobertura@latest
  - go test -coverprofile=${CI_PROJECT_DIR}/coverage.out -covermode count ${MODULE_NAME} ${MODULE_NAME}/utils
  - $GOPATH/bin/gocover-cobertura < ${CI_PROJECT_DIR}/coverage.out > ${CI_PROJECT_DIR}/coverage.xml
  - exit $?
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: "coverage.xml"
      junit: "test-report.xml"
  cache:
    paths:
    - ".cache"

image_build:
  stage: image_build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  only:
  - tags
  except:
  - branches
  - schedules
  script:
  - echo "R${CI_COMMIT_TAG} $(date -I) $CI_COMMIT_REF_NAME-$CI_COMMIT_SHA" > version.txt
  - mkdir -p /kaniko/.docker
  - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_REGISTRY_USER:$CI_JOB_TOKEN | base64)\"}}}" > /kaniko/.docker/config.json
  - /kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/Dockerfile --destination ${GLR_IMAGE_TAG_LATEST} --destination ${GLR_IMAGE_TAG_VERSION}

image_scan_dockle:
  stage: image_scan
  only:
  - tags
  except:
  - branches
  - schedules
  after_script:
  - cat ${ARTIFACT_FOLDER}/dockle_results.txt
  script:
  - export DOCKLE_LATEST=$(wget -q -O - https://api.github.com/repos/goodwithtech/dockle/releases/latest | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
  - wget https://github.com/goodwithtech/dockle/releases/download/v${DOCKLE_LATEST}/dockle_${DOCKLE_LATEST}_Linux-64bit.tar.gz && tar zxf dockle_${DOCKLE_LATEST}_Linux-64bit.tar.gz
  - ./dockle --output ${ARTIFACT_FOLDER}/dockle_results.txt ${GLR_IMAGE_TAG_VERSION}
  - ./dockle --exit-code 1 -f json --output ${ARTIFACT_FOLDER}/dockle_results.json ${GLR_IMAGE_TAG_VERSION}
  artifacts:
    when: always
    paths:
    - "${ARTIFACT_FOLDER}/dockle_results.txt"
    reports:
      container_scanning: "${ARTIFACT_FOLDER}/dockle_results.json"

.image_scan_trivy:
  stage: image_scan
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  after_script:
    - cat ${SCAN_ARTIFACT_FOLDER}/trivy_results.txt
  script:
    - trivy --cache-dir ${TRIVY_CACHE} image --no-progress --output ${SCAN_ARTIFACT_FOLDER}/trivy_results.txt ${SCAN_IMAGE}
    - trivy --cache-dir ${TRIVY_CACHE} image --no-progress --exit-code 1 --severity CRITICAL -f json --output ${SCAN_ARTIFACT_FOLDER}/trivy_results.json ${SCAN_IMAGE}
  artifacts:
    when: always
    paths:
      - "${SCAN_ARTIFACT_FOLDER}/trivy_results.txt"
    reports:
      container_scanning: "${SCAN_ARTIFACT_FOLDER}/trivy_results.json"
  cache:
    paths:
      - ".cache"

versioned_image_scan_trivy:
  extends: .image_scan_trivy
  variables:
    SCAN_ARTIFACT_FOLDER: "${ARTIFACT_FOLDER}"
    SCAN_IMAGE: "${GLR_IMAGE_TAG_VERSION}"
  only:
    - tags
  except:
    - branches
    - schedules

latest_image_scan_trivy:
  extends: .image_scan_trivy
  variables:
    SCAN_ARTIFACT_FOLDER: "${ARTIFACT_FOLDER}"
    SCAN_IMAGE: "${GLR_IMAGE_TAG_LATEST}"
  only:
  - schedules
  - web

html_report:
  stage: report
  image: python:3.13
  only:
  - tags
  except:
  - branches
  when: always
  script:
  - mkdir json
  - cp ${ARTIFACT_FOLDER}/*.json ./json/
  - pip install json2html
  - wget https://raw.githubusercontent.com/shad0wrunner/docker_cicd/master/convert_json_results.py
  - python ./convert_json_results.py
  artifacts:
    paths:
    - results.html
