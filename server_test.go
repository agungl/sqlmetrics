package main

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/agungl/sqlmetrics/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func Test_configureFromEnv(t *testing.T) {
	os.Clearenv()
	configureFromEnv()
	assert.Nil(t, zap.L().Check(zap.DebugLevel, "debugging"))

	assert.Nil(t, os.Setenv("VERBOSE", "1"))
	configureFromEnv()
	assert.NotNil(t, zap.L().Check(zap.DebugLevel, "debugging"))
}

func Test_setupMetrics(t *testing.T) {
	initLogger()
	os.Clearenv()
	assert.Nil(t, os.Setenv("CONFIG", "utils/test_config.yml"))
	setupMetrics()
	assert.Equal(t, 2, len(*configEntries))

	assert.Nil(t, os.Setenv("CONFIG", "utils/test_config_old_format.yml"))
	setupMetrics()
	assert.Equal(t, 1, len(*configEntries))
}

func Test_calcGlobalSleepTime(t *testing.T) {
	initLogger()
	os.Clearenv()
	sleepTime := calcGlobalSleepTime()
	assert.Equal(t, 1, sleepTime)

	assert.Nil(t, os.Setenv("SLEEP", "10"))
	sleepTime = calcGlobalSleepTime()
	assert.Equal(t, 10, sleepTime)

	assert.Nil(t, os.Setenv("SLEEP", "25"))
	sleepTime = calcGlobalSleepTime()
	assert.Equal(t, 25, sleepTime)
}

func Test_calcMetricSleepTime(t *testing.T) {
	initLogger()
	os.Clearenv()
	configEntries := &utils.ConfigEntries{}
	configEntries.ReadConfig("utils/test_config.yml")
	assert.Equal(t, 2, len(*configEntries))

	sleepTime := calcMetricSleepTime(calcGlobalSleepTime(), (*configEntries)[0])
	assert.Equal(t, 120, sleepTime)
	sleepTime = calcMetricSleepTime(calcGlobalSleepTime(), (*configEntries)[1])
	assert.Equal(t, 1800, sleepTime)

	(*configEntries)[1].Sleep = 0
	sleepTime = calcMetricSleepTime(calcGlobalSleepTime(), (*configEntries)[1])
	assert.Equal(t, 1, sleepTime)
	(*configEntries)[1].Sleep = -1
	sleepTime = calcMetricSleepTime(calcGlobalSleepTime(), (*configEntries)[1])
	assert.Equal(t, 1, sleepTime)
}

func Test_checkHealth(t *testing.T) {
	initLogger()
	db, err := sql.Open("pgx", "postgresql://localhost:5432")
	assert.NoError(t, err)
	dbInstance = db
	jsonStatus, code := checkHealth()
	assert.Equal(t, http.StatusServiceUnavailable, code)
	status := make(map[string]string)
	assert.NoError(t, json.Unmarshal(jsonStatus, &status))
	assert.Equal(t, "fail", status["status"])
	assert.Equal(t, "database connection mot available", status["output"])
}

func Test_healthHandler(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/health", nil)
	res := httptest.NewRecorder()
	healthHandler(res, req)
	assert.Equal(t, http.StatusOK, res.Code)
	assert.Equal(t, "application/health+json", res.Header().Get("Content-Type"))
	assert.Equal(t, "max-age=30", res.Header().Get("cache-control"))
}

func initLogger() {
	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	config.DisableCaller = true
	config.DisableStacktrace = true
	logger, _ := config.Build()
	zap.ReplaceGlobals(logger)
}
