# SQL-based Metrics Provider Changelog (functional)

#### 25.02.2025 - version 0.3.5
> Use Go 1.24, latest Go modules and base images
> Updated DB drivers

#### 20.12.2024 - version 0.3.4
> Use Go 1.23.4, latest Go modules and base images

#### 10.11.2024 - version 0.3.3
> Use Go 1.23.3, latest Go modules and base images

#### 13.09.2024 - version 0.3.2
> Use Go 1.23.1, latest Go modules and base images

#### 20.06.2024 - version 0.3.1
> Image based on oraclelinux-slim to avoid Alpine DNS problems on K8s

#### 19.06.2024 - version 0.3.0
> Image based on RHEL-minimal to avoid Alpine DNS problems on K8s
> Use Go 1.22.4, latest Go modules and base images

#### 14.05.2024 - version 0.2.9
> Use Go 1.22.3, latest Go modules and base images

#### 17.03.2024 - version 0.2.8
> Use Go 1.22.1, latest Go modules and base images

#### 02.03.2024 - version 0.2.7
> Use Go 1.22, latest Go modules and base images
> Updated DB drivers

#### 14.01.2024 - version 0.2.6
> Updated DB drivers
> Use latest Go modules and base images

#### 21.12.2023 - version 0.2.5
> Updated DB drivers
> Use latest Go modules and base images

#### 25.11.2023 - version 0.2.4
> Updated DB drivers
> Use latest Go modules and base images

#### 31.10.2023 - version 0.2.3
> Updated DB drivers
> Use latest Go modules and base images

#### 09.08.2023 - version 0.2.2
> Updated DB drivers
> Use Go 1.21 and latest Go modules and base images

#### 03.07.2023 - version 0.2.1
> Updated DB drivers
> Use latest Go modules and base images

#### 27.05.2023 - version 0.2.0
> Proper escaping of DB passwords
> Use latest Go modules and base images

#### 15.04.2023 - version 0.1.9
> Exporting metrics to a broker is no longer supported (was undocumented)
> Use Go 1.20 and latest Go modules

#### 22.03.2023 - version 0.1.8
> Build pipeline now without privileged containers

#### 17.02.2023 - version 0.1.7
> Use Go 1.19 

#### 05.01.2023 - version 0.1.6
> Support for MS SQL Server

#### 17.12.2022 - version 0.1.5
> Use Go 1.18 and newest modules 

#### 11.12.2022 - version 0.1.4
> Failover for Oracle based on github.com/sijms/go-ora v2.5.16

#### 02.07.2022 - version 0.1.3
> Much smaller image size based on Alpine now instead of OEL 7

#### 06.05.2022 - version 0.1.2
> Replacement of the Oracle driver to get rid of the need of the Instant Client

#### 16.05.2021 - version 0.1.1
> Support for /health endpoint

#### 01.01.2022 - version 0.1.0
> Initially released version with support for Oracle, MySQL and PostgreSQL databases
